import { createSlice ,PayloadAction } from "@reduxjs/toolkit";

export interface CustomerReservationsState {
    value : string[];
}

const initialState: CustomerReservationsState = {
    value : [] ,

}

export const customerSlice = createSlice({
    name: "customerReservations",
    initialState,
    reducers: {
        customerAddReservation : (state,action:PayloadAction<string>) =>{
            state.value.push(action.payload)
        },
        customerRemoveReservation : (state,action:PayloadAction<number>) =>{
            state.value.splice(action.payload,1)
        },
        
    },
});

export const {customerAddReservation , customerRemoveReservation} = customerSlice.actions;
export default customerSlice.reducer;