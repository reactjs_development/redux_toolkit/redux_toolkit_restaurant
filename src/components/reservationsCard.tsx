import { useDispatch } from "react-redux";
import { removeReservation } from "../features/reservationSlice";

interface ReservationCardType {
    name: string;
    index: number;
}

function ReservationCard({ name, index }: ReservationCardType) {
    const dispatch = useDispatch();
    return (
        <div
            className="reservation-card-container"
            onClick={() => {
                dispatch(removeReservation(index));
            }}
        >
            <p>{name}</p>

        </div>
    )
}

export default ReservationCard;