import { useDispatch } from "react-redux";
import { customerRemoveReservation } from "../features/customerSlice";
interface CustomerCardType {
    name: string;
    index: number;
}

function CustomerCard({ name, index }: CustomerCardType) {
    const dispatch = useDispatch();
    return (
        <div
            className="reservation-card-container"
            onClick={() => {
                dispatch(customerRemoveReservation(index));
            }}
        >
            <p>{name}</p>

        </div>
    )
}

export default CustomerCard;