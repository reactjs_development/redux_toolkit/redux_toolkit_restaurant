import { createSlice ,PayloadAction } from "@reduxjs/toolkit";

export interface ReservationsState {
    value : string[];
}

const initialState: ReservationsState = {
    value : [] ,

}


//Use createSlice to create the slice. The first argument is an object with the following properties:
      //name: a string that is the name of the slice, in this case it is "reservations".
      //initialState: the initial state that was defined earlier.
      //reducers: an object containing the action handlers (reducers) for the slice. There are two reducers defined here:
                //addReservation: adds a new reservation to the value array. The action payload is a string containing the reservation name.
                //removeReservation: removes a reservation from the value array. The action payload is an index of the reservation to be removed.
export const reservationSlice = createSlice({
    name: "reservations",
    initialState,
    reducers: {
        addReservation : (state,action:PayloadAction<string>) =>{
            state.value.push(action.payload)
        },
        removeReservation : (state,action:PayloadAction<number>) =>{
            state.value.splice(action.payload,1)
        },
        
    },
});

//Export the actions from the slice using reservationSlice.actions. 
//There are two actions exported: addReservation and removeReservation.
export const {addReservation , removeReservation} = reservationSlice.actions;

//Export the reducer using reservationSlice.reducer.
// This will be used to update the state of the store.
export default reservationSlice.reducer;