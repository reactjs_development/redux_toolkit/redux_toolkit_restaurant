import { configureStore } from "@reduxjs/toolkit";
import reservationsReducer from "../features/reservationSlice"
import customerReducer from "../features/customerSlice";


//the reservations key that maps to the reservationsReducer that was imported earlier.
export const store = configureStore({
reducer: {
    reservations : reservationsReducer,
    customerReservations : customerReducer,
    
}
});

//Export a type RootState that represents the state of the store, which is obtained by calling store.getState().
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
