import React, { useState } from "react";
import "./App.css";
import { useDispatch } from "react-redux";
import ReservationCard from "./components/reservationsCard";
import CustomerCard from "./components/customerCard";
import { useSelector } from "react-redux/es/exports";
import { RootState } from "./app/store";
import { addReservation } from "./features/reservationSlice";
import { customerAddReservation } from "./features/customerSlice";

function App() {
  const dispatch = useDispatch();
  const [reservationNameInput, setReservationNameInput] = useState("");
  const [customerNameInput, setCustomerNameInput] = useState("");


  //////
  const reservations = useSelector(
    (state: RootState) => state.reservations.value
  );

  const customer = useSelector(
    (state: RootState) => state.customerReservations.value

  );

  const handleAddReservations = () => {
    dispatch(addReservation(reservationNameInput));
  };

  const customerAddReservations = () => {
    dispatch(customerAddReservation(customerNameInput));
  };

  return (
    <div className="App">
      <div className="container">

        <div className="reservation-container">
          <div>
            <h5 className="reservation-header">Reservations</h5>
            <div className="reservation-cards-container">
              {reservations.map((name, index) => {
                return <ReservationCard name={name} index={index} />;
              })}
            </div>
          </div>
          <div className="reservation-input-container">
            <input
              value={reservationNameInput}
              onChange={(e) => {
                setReservationNameInput(e.target.value);
              }}
            />
            <button onClick={handleAddReservations}>Add</button>
          </div>
        </div>

        <div className="customer-food-container">
          <div className="customer-food-card-container">
            <p>Selena Gomez</p>
            <div className="customer-foods-container">
              <div className="customer-food"></div>
              <div className="customer-food-input-container">
                <input
                  value={customerNameInput}
                  onChange={(e) => {
                    setCustomerNameInput(e.target.value);
                  }}
                />
                <button onClick={customerAddReservations}>Add</button>
              </div>
            </div>
          </div>
        </div>
        <div>
          <div>
            <h5 className="reservation-header">Reservations</h5>
            <div className="reservation-cards-container">
              {customer.map((name, index) => {
                return <CustomerCard name={name} index={index} />;
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;